import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {

	private Snake peter;
	private Snake takis;
		
	@Before
	public void setUp() throws Exception {
		peter = new Snake("Peter S", 10, "coffee");
		takis = new Snake("Takis Z", 80, "vegetables");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void isHealthyTest() {
//		String healthyFood = "vegetables";
//		String favFood = peter.indexOf(Arrays.asList(Snake));
//		assertEquals(healthyFood,favFood);
//		
		boolean testtakis = takis.isHealthy();
		assertTrue(testtakis);
		
		boolean testpeter = peter.isHealthy();
		//assertTrue(testpeter);

	}
	
	@Test
	public void fitsInCageTest()
	{
		//cageLength=50
		
		//Cage for peter
		
		boolean petercageEquals = peter.fitsInCage(50);
		//	assertTrue(petercageEquals);
		
		boolean petercageLengthless = peter.fitsInCage(45);
		//assertTrue(petercageLengthless);
				
		boolean petercageLengthMore = peter.fitsInCage(55);
		assertTrue(petercageLengthMore);
		
		
		//cage for takis
		
		// Cage for Taskis 
		boolean TaskiscageLengthless = takis.fitsInCage(20);
		//	assertTrue(TaskiscageLengthless);  
		
		boolean TaskiscageLengthEquals = takis.fitsInCage(50);
		//	assertTrue(TaskiscageLengthEquals);
		
		boolean TaskiscageLengthMore = takis.fitsInCage(70);
		assertTrue(TaskiscageLengthMore);
		
		
		
	}
}
